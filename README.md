# Lets Get Blogged

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

## Development server

* Make sure LGC Rest API project is running.
* Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `npm test` to execute the unit tests via Jest.
