import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { AppComponent } from "./app.component";
import { CardListComponent } from "./components/card-list/card-list.component";
import { CardComponent } from "./components/card/card.component";
import { MatCardModule } from "@angular/material/card";
import { HttpClientModule } from "@angular/common/http";
import { SectionHeaderComponent } from "./components/section-header/section-header.component";
import { MostCommentedListComponent } from "./components/most-commented-list/most-commented-list.component";
import { PostsPageComponent } from "./components/posts-page/posts-page.component";
import { PostPageComponent } from "./components/post-page/post-page.component";

describe("AppComponent", () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatCardModule, HttpClientModule],
      declarations: [
        AppComponent,
        CardListComponent,
        CardComponent,
        SectionHeaderComponent,
        MostCommentedListComponent,
        PostsPageComponent,
        PostPageComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
  });

  it("should create the app", () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
