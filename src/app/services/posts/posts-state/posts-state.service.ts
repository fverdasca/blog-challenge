import { Injectable } from "@angular/core";
import { Post, Comment } from "../../../models/posts.model";
import { Observable, ReplaySubject, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class PostsStateService {
  public posts$: Observable<Post[]>;
  public post$: Observable<Post>;
  public comments$: Observable<Comment[]>;

  private postsSubject: Subject<Post[]>;
  private postSubject: ReplaySubject<Post>;
  private commentsSubject: ReplaySubject<Comment[]>;

  private postsState: Post[] = [];
  private postState: Post = {} as Post;
  private commentsState: Comment[] = [];

  constructor() {
    this.postsSubject = new Subject<Post[]>();
    this.posts$ = this.postsSubject.asObservable();

    this.postSubject = new ReplaySubject<Post>(1);
    this.post$ = this.postSubject.asObservable();

    this.commentsSubject = new ReplaySubject<Comment[]>(1);
    this.comments$ = this.commentsSubject.asObservable();
  }

  set posts(posts: Post[]) {
    this.postsState = posts;
    this.postsSubject.next(this.postsState);
  }

  set post(post: Post) {
    this.postState = post;
    this.postSubject.next(this.postState);
  }

  set comments(comments: Comment[]) {
    this.commentsState = comments;
    this.commentsSubject.next(this.commentsState);
  }
}
