import { TestBed } from "@angular/core/testing";

import { PostsStateService } from "./posts-state.service";
import { Post, Comment } from "../../../models/posts.model";
import { HttpClientModule } from "@angular/common/http";

describe("PostsStateService", () => {
  let service: PostsStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(PostsStateService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  describe("posts setter", () => {
    it("should update the postsState variable", () => {
      service["postsState"] = [{ id: 1 } as Post];

      service.posts = [{ id: 3 } as Post, { id: 5 } as Post, { id: 7 } as Post];

      const postsStateMappedById = service["postsState"].map(post => post.id);
      expect(postsStateMappedById).toEqual([3, 5, 7]);
    });

    it("should trigger a new subject for posts$", () => {
      service["postsSubject"].next = jest.fn();

      service.posts = [{ id: 3 } as Post, { id: 5 } as Post];

      expect(service["postsSubject"].next).toHaveBeenCalledWith([
        { id: 3 } as Post,
        { id: 5 } as Post,
      ]);
    });
  });

  describe("post setter", () => {
    it("should update the postState variable", () => {
      service["postState"] = { id: 13 } as Post;

      service.post = { id: 3 } as Post;

      expect(service["postState"]).toEqual({ id: 3 } as Post);
    });

    it("should trigger a new subject for post$", () => {
      service["postSubject"].next = jest.fn();

      service.post = { id: 3 } as Post;

      expect(service["postSubject"].next).toHaveBeenCalledWith({
        id: 3,
      } as Post);
    });
  });

  describe("comments setter", () => {
    it("should update the commentsState variable", () => {
      service["commentsState"] = [{ id: 10 } as Comment];

      service.comments = [
        { id: 30 } as Comment,
        { id: 50 } as Comment,
        { id: 70 } as Comment,
      ];

      const commentsStateMappedById = service["commentsState"].map(
        post => post.id
      );
      expect(commentsStateMappedById).toEqual([30, 50, 70]);
    });

    it("should trigger a new subject for comments$", () => {
      service["commentsSubject"].next = jest.fn();

      service.comments = [{ id: 30 } as Comment, { id: 50 } as Comment];

      expect(service["commentsSubject"].next).toHaveBeenCalledWith([
        { id: 30 } as Comment,
        { id: 50 } as Comment,
      ]);
    });
  });
});
