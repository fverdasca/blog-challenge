import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import {
  CommentsApiResponse,
  Post,
  PostCommentRequest,
  PostsApiResponse,
} from "../../../models/posts.model";

@Injectable({
  providedIn: "root",
})
export class PostsApiService {
  private backend = environment.backend_endpoint;

  constructor(private http: HttpClient) {}

  getPosts(): Observable<PostsApiResponse> {
    return this.http.get<PostsApiResponse>(`${this.backend}/posts`);
  }

  getPost(id: string): Observable<Post> {
    return this.http.get<Post>(`${this.backend}/posts/${id}`);
  }

  getComments(id: string): Observable<CommentsApiResponse> {
    return this.http.get<CommentsApiResponse>(
      `${this.backend}/posts/${id}/comments`
    );
  }

  getAllComments(): Observable<CommentsApiResponse> {
    return this.http.get<CommentsApiResponse>(`${this.backend}/comments`);
  }

  postComment(id: string, comment: PostCommentRequest): Observable<Post> {
    return this.http.post<Post>(
      `${this.backend}/posts/${id}/comments`,
      comment
    );
  }
}
