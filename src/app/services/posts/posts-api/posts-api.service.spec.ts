import { TestBed } from "@angular/core/testing";

import { PostsApiService } from "./posts-api.service";
import { HttpClientModule } from "@angular/common/http";

describe("PostsApiService", () => {
  let service: PostsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });
    service = TestBed.inject(PostsApiService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
