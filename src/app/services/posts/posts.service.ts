import { Injectable } from "@angular/core";
import { PostsApiService } from "./posts-api/posts-api.service";
import { PostsStateService } from "./posts-state/posts-state.service";
import { combineLatest, Observable } from "rxjs";
import {
  CommentsApiResponse,
  PostCommentRequest,
} from "../../models/posts.model";

@Injectable({
  providedIn: "root",
})
export class PostsService {
  constructor(
    private postsApiService: PostsApiService,
    private postsStateService: PostsStateService
  ) {}

  getAllPosts() {
    this.postsApiService.getPosts().subscribe(posts => {
      this.postsStateService.posts = posts;
    });
  }

  getAllComments(): Observable<CommentsApiResponse> {
    return this.postsApiService.getAllComments();
  }

  getPostWithComments(id: string) {
    combineLatest([
      this.postsApiService.getPost(id),
      this.postsApiService.getComments(id),
    ]).subscribe(([post, comments]) => {
      this.postsStateService.post = post;
      this.postsStateService.comments = comments;
    });
  }

  addCommentToPost(id: string, comment: PostCommentRequest) {
    this.postsApiService.postComment(id, comment).subscribe(() => {
      this.updateComments(id);
    });
  }

  private updateComments(id: string) {
    this.postsApiService.getComments(id).subscribe(comments => {
      this.postsStateService.comments = comments;
    });
  }
}
