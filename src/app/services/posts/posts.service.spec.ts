import { TestBed } from "@angular/core/testing";

import { PostsService } from "./posts.service";
import { HttpClientModule } from "@angular/common/http";
import { PostsApiService } from "./posts-api/posts-api.service";
import { PostsStateService } from "./posts-state/posts-state.service";
import { Observable, of } from "rxjs";
import {
  CommentsApiResponse,
  Post,
  PostsApiResponse,
  Comment,
  PostCommentRequest,
} from "../../models/posts.model";

interface PostsApiServiceMock {
  getPosts: () => Observable<PostsApiResponse>;
  getPost: (id: string) => Observable<Post>;
  getComments: (id: string) => Observable<CommentsApiResponse>;
  postComment: (id: string, comment: PostCommentRequest) => Observable<Post>;
}

describe("PostsService", () => {
  let service: PostsService;
  let postsApiServiceMock: PostsApiServiceMock;
  let postsStateService: PostsStateService;

  beforeEach(() => {
    postsApiServiceMock = {
      getPosts: () =>
        of([
          { id: 1 } as Post,
          { id: 2 } as Post,
          { id: 3 } as Post,
          { id: 4 } as Post,
        ]),
      getPost: (id: string) => of({ id: 10 } as Post),
      getComments: (id: string) =>
        of([
          { id: 13 } as Comment,
          { id: 14 } as Comment,
          { id: 15 } as Comment,
        ]),
      postComment: (id: string, comment: PostCommentRequest) =>
        of({ id: 7 } as Post),
    };

    TestBed.configureTestingModule({
      providers: [
        PostsService,
        { provide: PostsApiService, useValue: postsApiServiceMock },
        PostsStateService,
      ],
      imports: [HttpClientModule],
    });
    service = TestBed.inject(PostsService);
    postsStateService = TestBed.inject(PostsStateService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  describe("getAllPosts method", () => {
    it("should call api service method", () => {
      const spy = jest.spyOn(postsApiServiceMock, "getPosts");

      service.getAllPosts();

      expect(spy).toHaveBeenCalled();
    });

    it("should update posts on api response", () => {
      const spy = jest.spyOn(postsStateService, "posts", "set");

      service.getAllPosts();

      expect(spy).toHaveBeenCalledWith([
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
      ]);
    });
  });

  describe("getPostWithComments method", () => {
    it("should call both getPost and getComments with the same id", () => {
      const spyPost = jest.spyOn(postsApiServiceMock, "getPost");
      const spyComments = jest.spyOn(postsApiServiceMock, "getComments");

      service.getPostWithComments("4");

      expect(spyPost).toHaveBeenCalledWith("4");
      expect(spyComments).toHaveBeenCalledWith("4");
    });

    it("should update the post on state service", () => {
      const spy = jest.spyOn(postsStateService, "post", "set");

      service.getPostWithComments("mocked_id");

      // this expect with id 10 is related to the api response mock at line 35
      expect(spy).toHaveBeenCalledWith({ id: 10 });
    });

    it("should update the comments on state service", () => {
      const spy = jest.spyOn(postsStateService, "comments", "set");

      service.getPostWithComments("mocked_id");

      // this expect is related to the api response mock at line 36
      expect(spy).toHaveBeenCalledWith([{ id: 13 }, { id: 14 }, { id: 15 }]);
    });
  });

  describe("addCommentToPost method", () => {
    it("should call the updateComments method", () => {
      service["updateComments"] = jest.fn();

      service.addCommentToPost("mocked_id", {} as PostCommentRequest);

      // this expect is related to the api response mock at line 36
      expect(service["updateComments"]).toHaveBeenCalledWith("mocked_id");
    });
  });
});
