function formatTwoDigits(n: number) {
  return (n < 10 ? "0" : "") + n;
}

export function getCurrentTimeFormatted(): string {
  const date = new Date();
  const month = formatTwoDigits(date.getMonth() + 1);
  const day = formatTwoDigits(date.getDate());
  const year = date.getFullYear();

  return year + "-" + month + "-" + day;
}
