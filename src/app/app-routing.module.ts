import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PostsPageComponent } from "./components/posts-page/posts-page.component";
import { PostPageComponent } from "./components/post-page/post-page.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";

const routes: Routes = [
  { path: "posts", component: PostsPageComponent },
  { path: "posts/:id", component: PostPageComponent },
  { path: "", redirectTo: "/posts", pathMatch: "full" },
  { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
