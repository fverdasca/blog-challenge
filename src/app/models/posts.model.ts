export interface Comment {
  id: number;
  postId: number | string;
  parent_id: string;
  user: string;
  date: string;
  content: string;
}

export interface Post {
  id: number;
  title: string;
  author: string;
  publish_date: string;
  slug: string;
  description: string;
  content: string;
}

export interface PostCommentRequest {
  user: string;
  date: string;
  content: string;
}

export type PostsApiResponse = Post[];
export type CommentsApiResponse = Comment[];
