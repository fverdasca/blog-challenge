import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material.module";
import { CardListComponent } from "./components/card-list/card-list.component";
import { CardComponent } from "./components/card/card.component";
import { HttpClientModule } from "@angular/common/http";
import { SectionHeaderComponent } from "./components/section-header/section-header.component";
import { PostsPageComponent } from "./components/posts-page/posts-page.component";
import { MostCommentedListComponent } from "./components/most-commented-list/most-commented-list.component";
import { PostPageComponent } from "./components/post-page/post-page.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { SinglePostCardComponent } from './components/single-post-card/single-post-card.component';
import { SinglePostCommentsComponent } from './components/single-post-comments/single-post-comments.component';

@NgModule({
  declarations: [
    AppComponent,
    CardListComponent,
    CardComponent,
    PostsPageComponent,
    PostPageComponent,
    MostCommentedListComponent,
    SectionHeaderComponent,
    PageNotFoundComponent,
    SinglePostCardComponent,
    SinglePostCommentsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
