import { ComponentFixture, TestBed } from "@angular/core/testing";

import { MostCommentedListComponent } from "./most-commented-list.component";
import { SectionHeaderComponent } from "../section-header/section-header.component";
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";

describe("MostCommentedListComponent", () => {
  let component: MostCommentedListComponent;
  let fixture: ComponentFixture<MostCommentedListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MostCommentedListComponent, SectionHeaderComponent],
      imports: [HttpClientModule, RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(MostCommentedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
