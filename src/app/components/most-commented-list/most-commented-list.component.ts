import { Component, OnInit } from "@angular/core";
import { PostsStateService } from "../../services/posts/posts-state/posts-state.service";
import { combineLatestWith, map } from "rxjs";
import { PostsService } from "../../services/posts/posts.service";
import { Comment, Post } from "../../models/posts.model";
import { Router } from "@angular/router";

interface PostWithComments extends Post {
  numberOfComments: number;
}

@Component({
  selector: "app-most-commented-list",
  templateUrl: "./most-commented-list.component.html",
  styleUrls: ["./most-commented-list.component.scss"],
})
export class MostCommentedListComponent implements OnInit {
  mostCommentedPosts: PostWithComments[] = [];

  constructor(
    private postsService: PostsService,
    private postsStateService: PostsStateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.postsStateService.posts$
      .pipe(
        combineLatestWith(this.postsService.getAllComments()),
        map(([posts, comments]) => {
          const postsWithSumOfComments = this.addSumOfComments(posts, comments);
          const onlyPostsWithComments = this.filterPostsOnlyWithComments(
            postsWithSumOfComments
          );
          return onlyPostsWithComments.sort((a: Post, b: Post) => {
            const first = this.numberOfComments(a.id, comments);
            const second = this.numberOfComments(b.id, comments);
            return first > second ? -1 : 1;
          });
        })
      )
      .subscribe(value => {
        this.mostCommentedPosts = value;
      });
  }

  openPost(id: number) {
    if (id) {
      this.router.navigate(["/posts", id]);
    }
  }

  private filterPostsOnlyWithComments(
    posts: PostWithComments[]
  ): PostWithComments[] {
    return posts.filter(post => post.numberOfComments > 0);
  }

  private addSumOfComments(
    posts: Post[],
    comments: Comment[]
  ): PostWithComments[] {
    return posts.map(post => ({
      ...post,
      numberOfComments: this.numberOfComments(post.id, comments),
    }));
  }

  private numberOfComments(id: number | string, comments: Comment[]): number {
    return comments.filter(
      comment => comment.postId.toString() === id.toString()
    ).length;
  }
}
