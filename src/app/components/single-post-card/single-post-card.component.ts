import { Component, OnDestroy, OnInit } from "@angular/core";
import { Post } from "../../models/posts.model";
import { Subscription } from "rxjs";
import { PostsStateService } from "../../services/posts/posts-state/posts-state.service";

@Component({
  selector: "app-single-post-card",
  templateUrl: "./single-post-card.component.html",
  styleUrls: ["./single-post-card.component.scss"],
})
export class SinglePostCardComponent implements OnInit, OnDestroy {
  public post!: Post;

  private subscription: Subscription;

  constructor(private postsStateService: PostsStateService) {
    this.subscription = new Subscription();
  }

  ngOnInit() {
    this.subscription.add(this.subscribeToPost());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private subscribeToPost(): Subscription {
    return this.postsStateService.post$.subscribe(post => {
      this.post = post;
    });
  }
}
