import { ComponentFixture, TestBed } from "@angular/core/testing";

import { SinglePostCardComponent } from "./single-post-card.component";
import { MatCardModule } from "@angular/material/card";

describe("SinglePostCardComponent", () => {
  let component: SinglePostCardComponent;
  let fixture: ComponentFixture<SinglePostCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SinglePostCardComponent],
      imports: [MatCardModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SinglePostCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
