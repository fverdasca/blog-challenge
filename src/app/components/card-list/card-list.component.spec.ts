import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CardListComponent } from "./card-list.component";
import { CardComponent } from "../card/card.component";
import { MatCardModule } from "@angular/material/card";
import { HttpClientModule } from "@angular/common/http";
import { PostsService } from "../../services/posts/posts.service";
import { SectionHeaderComponent } from "../section-header/section-header.component";

describe("CardListComponent", () => {
  let postsService: PostsService;
  let component: CardListComponent;
  let fixture: ComponentFixture<CardListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardListComponent, CardComponent, SectionHeaderComponent],
      providers: [PostsService],
      imports: [MatCardModule, HttpClientModule],
    }).compileComponents();

    postsService = TestBed.inject(PostsService);
    fixture = TestBed.createComponent(CardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should call getAllPosts from postsService on init", () => {
    const spy = jest.spyOn(postsService, "getAllPosts");

    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });
});
