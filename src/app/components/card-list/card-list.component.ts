import { Component, OnInit } from "@angular/core";
import { PostsService } from "../../services/posts/posts.service";
import { PostsStateService } from "../../services/posts/posts-state/posts-state.service";
import { Observable } from "rxjs";
import { Post } from "../../models/posts.model";

@Component({
  selector: "app-card-list",
  templateUrl: "./card-list.component.html",
  styleUrls: ["./card-list.component.scss"],
})
export class CardListComponent implements OnInit {
  posts$: Observable<Post[]>;

  constructor(
    private postsService: PostsService,
    private postsStateService: PostsStateService
  ) {
    this.posts$ = this.postsStateService.posts$;
  }

  ngOnInit() {
    this.postsService.getAllPosts();
  }
}
