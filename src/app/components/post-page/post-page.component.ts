import { Component, OnInit } from "@angular/core";
import { PostsService } from "../../services/posts/posts.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-post-page",
  templateUrl: "./post-page.component.html",
  styleUrls: ["./post-page.component.scss"],
})
export class PostPageComponent implements OnInit {
  constructor(
    private postsService: PostsService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params["id"];
    this.postsService.getPostWithComments(id);
  }
}
