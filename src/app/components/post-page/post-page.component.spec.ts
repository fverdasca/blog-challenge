import { ComponentFixture, TestBed } from "@angular/core/testing";

import { PostPageComponent } from "./post-page.component";
import { HttpClientModule } from "@angular/common/http";
import { RouterTestingModule } from "@angular/router/testing";
import { SinglePostCardComponent } from "../single-post-card/single-post-card.component";
import { SinglePostCommentsComponent } from "../single-post-comments/single-post-comments.component";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatListModule } from "@angular/material/list";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe("PostPageComponent", () => {
  let component: PostPageComponent;
  let fixture: ComponentFixture<PostPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostPageComponent,
        SinglePostCardComponent,
        SinglePostCommentsComponent,
      ],
      providers: [],
      imports: [
        HttpClientModule,
        RouterTestingModule,
        MatCardModule,
        MatInputModule,
        ReactiveFormsModule,
        MatListModule,
        BrowserAnimationsModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PostPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
