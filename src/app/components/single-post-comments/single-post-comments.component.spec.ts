import { ComponentFixture, TestBed } from "@angular/core/testing";

import { SinglePostCommentsComponent } from "./single-post-comments.component";
import { MatCardModule } from "@angular/material/card";
import { MatInputModule } from "@angular/material/input";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatListModule } from "@angular/material/list";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { PostsService } from "../../services/posts/posts.service";
import { RouterTestingModule } from "@angular/router/testing";

describe("SinglePostCommentsComponent", () => {
  let component: SinglePostCommentsComponent;
  let fixture: ComponentFixture<SinglePostCommentsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SinglePostCommentsComponent],
      providers: [PostsService],
      imports: [
        MatCardModule,
        MatInputModule,
        MatListModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterTestingModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(SinglePostCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
