import { Component } from "@angular/core";
import { Comment } from "../../models/posts.model";
import { PostsStateService } from "../../services/posts/posts-state/posts-state.service";
import { map, Observable } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { getCurrentTimeFormatted } from "../../utils/date.utils";
import { PostsService } from "../../services/posts/posts.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-single-post-comments",
  templateUrl: "./single-post-comments.component.html",
  styleUrls: ["./single-post-comments.component.scss"],
})
export class SinglePostCommentsComponent {
  public comments$: Observable<Comment[]>;
  public commentForm = new FormGroup({
    nameForm: new FormControl("", [Validators.required]),
    commentForm: new FormControl("", [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  private id;

  constructor(
    private postsStateService: PostsStateService,
    private postsService: PostsService,
    private activatedRoute: ActivatedRoute
  ) {
    this.id = this.activatedRoute.snapshot.params["id"];
    this.comments$ = this.postsStateService.comments$.pipe(
      map(comment => {
        return comment.sort((a, b) => {
          const aUnix = +new Date(a.date);
          const bUnix = +new Date(b.date);
          return aUnix >= bUnix ? -1 : 1;
        });
      })
    );
  }

  private resetForm() {
    this.commentForm.reset();
  }

  insertComment() {
    if (this.commentForm.status === "VALID") {
      const user = this.commentForm.get("nameForm")?.value;
      const content = this.commentForm.get("commentForm")?.value;
      const date = getCurrentTimeFormatted();

      this.postsService.addCommentToPost(this.id, {
        user,
        date,
        content,
      });

      this.resetForm();
    }
  }
}
