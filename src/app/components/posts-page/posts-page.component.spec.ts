import { ComponentFixture, TestBed } from "@angular/core/testing";

import { PostsPageComponent } from "./posts-page.component";
import { CardListComponent } from "../card-list/card-list.component";
import { MostCommentedListComponent } from "../most-commented-list/most-commented-list.component";
import { HttpClientModule } from "@angular/common/http";
import { SectionHeaderComponent } from "../section-header/section-header.component";
import { RouterTestingModule } from "@angular/router/testing";

describe("PostsPageComponent", () => {
  let component: PostsPageComponent;
  let fixture: ComponentFixture<PostsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        PostsPageComponent,
        CardListComponent,
        MostCommentedListComponent,
        SectionHeaderComponent,
      ],
      imports: [HttpClientModule, RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(PostsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
