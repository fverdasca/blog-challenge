import { Component, Input } from "@angular/core";
import { Post } from "../../models/posts.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.scss"],
})
export class CardComponent {
  @Input() post: Post | undefined;

  constructor(private router: Router) {}

  openCard(id: number | undefined) {
    if (id) {
      this.router.navigate(["/posts", id]);
    }
  }
}
